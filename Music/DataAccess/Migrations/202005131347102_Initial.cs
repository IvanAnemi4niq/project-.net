﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Albums",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        YearReleased = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Album_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Albums", t => t.Album_ID)
                .Index(t => t.Album_ID);
            
            CreateTable(
                "dbo.Artists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 64),
                        LastName = c.String(nullable: false, maxLength: 64),
                        Age = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Songs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        Duration = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        Album_ID = c.Int(),
                        Style_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Albums", t => t.Album_ID)
                .ForeignKey("dbo.Styles", t => t.Style_ID)
                .Index(t => t.Album_ID)
                .Index(t => t.Style_ID);
            
            CreateTable(
                "dbo.Styles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SongArtists",
                c => new
                    {
                        Song_ID = c.Int(nullable: false),
                        Artist_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Song_ID, t.Artist_ID })
                .ForeignKey("dbo.Songs", t => t.Song_ID, cascadeDelete: true)
                .ForeignKey("dbo.Artists", t => t.Artist_ID, cascadeDelete: true)
                .Index(t => t.Song_ID)
                .Index(t => t.Artist_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Songs", "Style_ID", "dbo.Styles");
            DropForeignKey("dbo.SongArtists", "Artist_ID", "dbo.Artists");
            DropForeignKey("dbo.SongArtists", "Song_ID", "dbo.Songs");
            DropForeignKey("dbo.Songs", "Album_ID", "dbo.Albums");
            DropForeignKey("dbo.Albums", "Album_ID", "dbo.Albums");
            DropIndex("dbo.SongArtists", new[] { "Artist_ID" });
            DropIndex("dbo.SongArtists", new[] { "Song_ID" });
            DropIndex("dbo.Songs", new[] { "Style_ID" });
            DropIndex("dbo.Songs", new[] { "Album_ID" });
            DropIndex("dbo.Albums", new[] { "Album_ID" });
            DropTable("dbo.SongArtists");
            DropTable("dbo.Styles");
            DropTable("dbo.Songs");
            DropTable("dbo.Artists");
            DropTable("dbo.Albums");
        }
    }
}
