﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Requirements : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Artists", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Artists", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Songs", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Songs", "Name", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Artists", "LastName", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Artists", "FirstName", c => c.String(nullable: false, maxLength: 64));
        }
    }
}
