﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Age : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Artists", "Age");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Artists", "Age", c => c.Int(nullable: false));
        }
    }
}
