﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Albums", "Album_ID", "dbo.Albums");
            DropIndex("dbo.Albums", new[] { "Album_ID" });
            DropColumn("dbo.Albums", "Album_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Albums", "Album_ID", c => c.Int());
            CreateIndex("dbo.Albums", "Album_ID");
            AddForeignKey("dbo.Albums", "Album_ID", "dbo.Albums", "ID");
        }
    }
}
