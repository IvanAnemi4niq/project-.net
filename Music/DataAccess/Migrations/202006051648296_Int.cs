﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Int : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Albums", "YearReleased", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Albums", "YearReleased", c => c.String(nullable: false));
        }
    }
}
