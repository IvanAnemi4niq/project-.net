﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Revert : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AlbumSongs", "Album_ID", "dbo.Albums");
            DropForeignKey("dbo.AlbumSongs", "Song_ID", "dbo.Songs");
            DropIndex("dbo.AlbumSongs", new[] { "Album_ID" });
            DropIndex("dbo.AlbumSongs", new[] { "Song_ID" });
            DropTable("dbo.AlbumSongs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AlbumSongs",
                c => new
                    {
                        Album_ID = c.Int(nullable: false),
                        Song_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Album_ID, t.Song_ID });
            
            CreateIndex("dbo.AlbumSongs", "Song_ID");
            CreateIndex("dbo.AlbumSongs", "Album_ID");
            AddForeignKey("dbo.AlbumSongs", "Song_ID", "dbo.Songs", "ID", cascadeDelete: true);
            AddForeignKey("dbo.AlbumSongs", "Album_ID", "dbo.Albums", "ID", cascadeDelete: true);
        }
    }
}
