﻿namespace DataAccess.Repositories
{
    using DataStructure;

    interface IGenericRepository<T> where T : Model
    {
        T GetByID(int id);
        void Delete(int id);

        void CreateOrUpdate(T entity);
    }
}
