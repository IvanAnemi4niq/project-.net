﻿using DataStructure;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Repositories
{
    public class StyleRepository : GenericRepository<Style>
    {
        public StyleRepository(DBContext context) : base(context) { }

        public List<Style> SearchBy(string queryString)
        {
            return context.Styles.Where(style => style.Title.Contains(queryString)).ToList();
        }
    }
}
