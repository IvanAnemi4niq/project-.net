﻿namespace DataAccess.Repositories
{
    using DataStructure;
    using System.Collections.Generic;
    using System.Linq;

    public class ArtistRepository : GenericRepository<Artist>
    {
        public ArtistRepository() : base(new DBContext())
        { }

       
       
        public List<Artist> SearchBy(string queryString)
        {
            return context.Artists.Where(artist => artist.FirstName.Contains(queryString) || artist.LastName.Contains(queryString)).ToList();
        }

        public List<Artist> GetByIDs(List<int> IDs)
        {
            List<Artist> artists = new List<Artist>();
            foreach (int ID in IDs)
            {
                artists.Add(GetByID(ID));
            }
            return artists;
        }
    }
}