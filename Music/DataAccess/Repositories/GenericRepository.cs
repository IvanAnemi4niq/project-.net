﻿namespace DataAccess.Repositories
{
    using DataStructure;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public abstract class GenericRepository<T> : IDisposable, IGenericRepository<T> where T : Model
    {
        private bool disposed = false;


        public readonly DBContext context;


        readonly DbSet<T> entities;

        public GenericRepository(DBContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }


        public List<T> GetAll()
        {
            return entities.ToList();
        }


        public virtual void CreateOrUpdate(T entity)
        {
            DateTime currentDateTime = DateTime.Now;


            if (entity.ID == 0)
            {

                entity.CreatedAt = currentDateTime;
            }
            else
            {
                entity.CreatedAt = GetByID(entity.ID).CreatedAt;
            }

            entity.UpdatedAt = currentDateTime;
           

            entities.AddOrUpdate(entity);

            context.SaveChanges();
        }


        public void Delete(int id)
        {

            T entity = GetByID(id);
            if (entity != null)
            {

                entities.Remove(entity);

                context.SaveChanges();
            }
        }


        public T GetByID(int id)
        {
            return entities.Find(id);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                context.Dispose();
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(obj: this);
        }

    }
}
