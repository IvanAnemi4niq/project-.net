﻿namespace DataAccess.Repositories
{
    using DataStructure;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class SongRepository : GenericRepository<Song>
    {
        public SongRepository(DBContext context) : base(context) { }

        public override void CreateOrUpdate(Song entity)
        {
            DateTime currentDateTime = DateTime.Now;


            if (entity.ID == 0)
            {
                entity.CreatedAt = currentDateTime;
                List<int> artistIDs = new List<int>();
                foreach (Artist artist in entity.Artists)
                    artistIDs.Add(artist.ID);
                entity.Artists.Clear();
                foreach (int ID in artistIDs)
                    entity.Artists.Add(context.Artists.Find(ID));
            }
            else
            {
                Song row = GetByID(entity.ID);
                entity.CreatedAt = row.CreatedAt;
                if (row.Album != entity.Album)
                    row.Album = entity.Album;
                if (row.Style != entity.Style)
                    row.Style = entity.Style;
                row.Artists.Clear();
                foreach (Artist artist in entity.Artists)
                    row.Artists.Add(context.Artists.Find(artist.ID));
            }
            entity.UpdatedAt = currentDateTime;


            context.Songs.AddOrUpdate(entity);
            context.SaveChanges();
        }

        public List<Song> SearchBy(string queryString)
        {
            return this.context.Songs.Where(song => song.Name.Contains(queryString)).ToList();
        }
    }
}
