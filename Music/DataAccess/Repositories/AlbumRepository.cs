﻿using DataStructure;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Repositories
{
    public class AlbumRepository : GenericRepository<Album>
    {
        public AlbumRepository(DBContext context) : base(context) { }

        public List<Album> SearchBy(string queryString)
        {
            return context.Albums.Where(album => album.Name.Contains(queryString)).ToList();
        }
    }
}
