﻿using System;

namespace DataAccess.Repositories
{
    public class UnitOfWork
    {
        private DBContext context;

        private ArtistRepository artistRepository;
        private SongRepository songRepository;
        private StyleRepository styleRepository;
        private AlbumRepository albumRepository;

        private static UnitOfWork main;
        public static UnitOfWork Main
        {
            get
            {
                if (main == null)
                {
                    main = new UnitOfWork();
                }
                return main;
            }
        }

        public StyleRepository StyleRepository
        {
            get
            {
                if (this.styleRepository == null)
                {
                    this.styleRepository = new StyleRepository(context);
                }
                return styleRepository;
            }
        }

        public AlbumRepository AlbumRepository
        {
            get
            {
                if (this.albumRepository == null)
                {
                    this.albumRepository = new AlbumRepository(context);
                }
                return albumRepository;
            }
        }

        public SongRepository SongRepository
        {
            get
            {
                if (this.songRepository == null)
                {
                    this.songRepository = new SongRepository(context);
                }

                return songRepository;
            }
        }

        public ArtistRepository ArtistRepository
        {
            get
            {
                if (this.artistRepository == null)
                {
                    this.artistRepository = new ArtistRepository();
                }

                return this.artistRepository;
            }
        }

        private UnitOfWork()
        {
            this.context = new DBContext();
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (context == null) return;

            context.Dispose();
            context = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
