﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DataStructure;

namespace DataAccess
{
    public partial class DBContext : DbContext
    {


        // Connects to Songs table
        public virtual DbSet<Song> Songs { get; set; }

        // Connects to Artists table
        public virtual DbSet<Artist> Artists { get; set; }


        //Connects to Album table
        public virtual DbSet<Album> Albums { get; set; }

        // Connects to Style table

        public virtual DbSet<Style> Styles { get; set; }

        //Constructor

        public DBContext()
            : base("DBContext")
        {
        }




        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Song>()
                .HasMany<Artist>(song => song.Artists)
                .WithMany(artist => artist.Songs)
                .Map(cs =>
                {
                    cs.MapLeftKey("Song_ID");
                    cs.MapRightKey("Artist_ID");
                    cs.ToTable("SongArtists");
                });
        }
    }
}
