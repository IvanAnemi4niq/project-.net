﻿namespace Music.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using DataAccess.Repositories;
    using DataStructure;
    using Music.Models.ViewModels;
    using MvcPaging;

    public class SongsController : BaseController<Song>
    {
        public SongsController() : base(UnitOfWork.Main.SongRepository) { }

        public ActionResult Index(int? page, string queryString)
        {
            List<Song> songs;
            if (queryString != null && queryString != "")
            {
                songs = uow.SongRepository.SearchBy(queryString);
            }
            else
            {
                songs = uow.SongRepository.GetAll();
            }

            List<SongViewModel> songViewModels = SongViewModel.ToList(songs);

            int maxPages = this.CalculatePagesCount(songViewModels);
            int currentPage = this.CalculateCurrentPageNumber(page, maxPages);

            ViewBag.Page = currentPage;
            ViewBag.Max = maxPages;

            return View(songViewModels.ToPagedList(currentPage, pageSize));
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Song song = uow.SongRepository.GetByID((int)id);
            if (song == null)
            {
                return HttpNotFound();
            }
            SongViewModel songViewModel = new SongViewModel(song);
            return View(songViewModel);
        }

        public ActionResult Create()
        {
            List<StyleViewModel> styles = StyleViewModel.ToList(uow.StyleRepository.GetAll());
            ViewBag.Styles = new SelectList(styles, "ID", "Title");

            List<AlbumViewModel> albums = AlbumViewModel.ToList(uow.AlbumRepository.GetAll());
            ViewBag.Albums = new SelectList(albums, "ID", "Name");

            List<ArtistViewModel> artists = ArtistViewModel.ToList(uow.ArtistRepository.GetAll());
            ViewBag.Artists = new MultiSelectList(artists, "ID", "FullName");

            return View();
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Song song = uow.SongRepository.GetByID((int)id);
            if (song == null)
            {
                return HttpNotFound();
            }
            SongViewModel songViewModel = new SongViewModel(song);

            List<StyleViewModel> styles = StyleViewModel.ToList(uow.StyleRepository.GetAll());
            ViewBag.Styles = new SelectList(styles, "ID", "Title", songViewModel.StyleID);

            List<AlbumViewModel> albums = AlbumViewModel.ToList(uow.AlbumRepository.GetAll());
            ViewBag.Albums = new SelectList(albums, "ID", "Name", selectedValue: songViewModel.AlbumID);

            List<ArtistViewModel> artists = ArtistViewModel.ToList(uow.ArtistRepository.GetAll());
            ViewBag.Artists = new MultiSelectList(artists, "ID", "FullName", songViewModel.ArtistIDs);

            return View(songViewModel);
        }

        //[Authorize(Roles = "admin")]
        // GET: Actors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Song song = uow.SongRepository.GetByID((int)id);
            if (song == null)
            {
                return HttpNotFound();
            }
            SongViewModel songViewModel = new SongViewModel(song);
            return View(songViewModel);
        }

        // POST: Films/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Duration,StyleID,AlbumID,ArtistIDs,CreatedAt,UpdatedAt")] SongViewModel songViewModel)
        {
            if (ModelState.IsValid)
            {
                Song song = Transform(songViewModel);
                uow.SongRepository.CreateOrUpdate(song);
                return RedirectToAction("Index");
            }

            return View(songViewModel);
        }

        // POST: Films/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Duration,StyleID,AlbumID,ArtistIDs,CreatedAt,UpdatedAt")] SongViewModel songViewModel)
        {
            if (ModelState.IsValid)
            {
                Song song = Transform(songViewModel);
                uow.SongRepository.CreateOrUpdate(song);
                return RedirectToAction("Index");
            }
            return View(songViewModel);
        }

        private Song Transform(SongViewModel songViewModel)
        {
            return new Song()
            {
                ID = songViewModel.ID,
                Name = songViewModel.Name,
                Style = uow.StyleRepository.GetByID(songViewModel.StyleID),
                Album = uow.AlbumRepository.GetByID(songViewModel.AlbumID),
                CreatedAt = songViewModel.CreatedAt,
                UpdatedAt = songViewModel.UpdatedAt,
                Artists = uow.ArtistRepository.GetByIDs(songViewModel.ArtistIDs),
                Duration = songViewModel.Duration
            };
        }

    }
}
