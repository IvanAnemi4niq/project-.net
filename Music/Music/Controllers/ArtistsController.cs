﻿namespace Music.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using DataAccess.Repositories;
    using DataStructure;
    using Music.Models.ViewModels;
    using MvcPaging;

    //[Authorize(Roles = "admin")]
    public class ArtistsController : BaseController<Artist>
    {
        public ArtistsController() : base(UnitOfWork.Main.ArtistRepository) { }

        public ActionResult Index(int? page, string queryString)
        {
            List<Artist> artists;

            if (queryString != null && queryString != "")
            {
                artists = uow.ArtistRepository.SearchBy(queryString);
            }
            else
            {
                artists = uow.ArtistRepository.GetAll();
            }

            List<ArtistViewModel> artistViewModels = ArtistViewModel.ToList(artists);

            int maxPages = this.CalculatePagesCount(artistViewModels);
            int currentPage = this.CalculateCurrentPageNumber(page, maxPages);

            ViewBag.Page = currentPage;
            ViewBag.Max = maxPages;

            return View(artistViewModels.ToPagedList(currentPage, pageSize));
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artist artist = uow.ArtistRepository.GetByID((int)id);
            if (artist == null)
            {
                return HttpNotFound();
            }
            ArtistViewModel artistViewModel = new ArtistViewModel(artist);
            return View(artistViewModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artist artist = uow.ArtistRepository.GetByID((int)id);
            if (artist == null)
            {
                return HttpNotFound();
            }
            ArtistViewModel artistViewModel = new ArtistViewModel(artist);
            return View(artistViewModel);
        }

        //[Authorize(Roles = "admin")]
        // GET: Actors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artist artist = uow.ArtistRepository.GetByID((int)id);
            if (artist == null)
            {
                return HttpNotFound();
            }
            ArtistViewModel artistViewModel = new ArtistViewModel(artist);
            return View(artistViewModel);
        }

        // POST: Actors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FirstName,LastName,CreatedAt,UpdatedAt")] ArtistViewModel artistViewModel)
        {
            if (ModelState.IsValid)
            {
                Artist artist = Transform(artistViewModel);
                uow.ArtistRepository.CreateOrUpdate(artist);
                return RedirectToAction("Index");
            }
            return View(artistViewModel);
        }

        // POST: Actors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName,CreatedAt,UpdatedAt")] ArtistViewModel artistViewModel)
        {
            if (ModelState.IsValid)
            {
                Artist artist = Transform(artistViewModel);
                uow.ArtistRepository.CreateOrUpdate(artist);
                return RedirectToAction("Index");
            }
            return View(artistViewModel);
        }

        public Artist Transform(ArtistViewModel artistViewModel)
        {
            return new Artist()
            {
                ID = artistViewModel.ID,
                FirstName = artistViewModel.FirstName,
                LastName = artistViewModel.LastName,
                CreatedAt = artistViewModel.CreatedAt,
                UpdatedAt = artistViewModel.UpdatedAt
            };
        }
    }
}
