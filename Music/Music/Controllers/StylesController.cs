﻿namespace Music.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using DataAccess.Repositories;
    using DataStructure;
    using Music.Models.ViewModels;
    using MvcPaging;

    //[Authorize(Roles = "admin")]
    public class StylesController : BaseController<Style>
    {
        public StylesController() : base(UnitOfWork.Main.StyleRepository) { }

        public ActionResult Index(int? page, string queryString)
        {
            List<Style> styles;

            if (queryString != null && queryString != "")
            {
                styles = uow.StyleRepository.SearchBy(queryString);
            }
            else
            {
                styles = uow.StyleRepository.GetAll();
            }

            List<StyleViewModel> styleViewModels = StyleViewModel.ToList(styles);

            int maxPages = this.CalculatePagesCount(styleViewModels);
            int currentPage = this.CalculateCurrentPageNumber(page, maxPages);

            ViewBag.Page = currentPage;
            ViewBag.Max = maxPages;

            return View(styleViewModels.ToPagedList(currentPage, pageSize));
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Style style = uow.StyleRepository.GetByID((int)id);
            if (style == null)
            {
                return HttpNotFound();
            }
            StyleViewModel styleViewModel = new StyleViewModel(style);
            return View(styleViewModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Style style = uow.StyleRepository.GetByID((int)id);
            if (style == null)
            {
                return HttpNotFound();
            }
            StyleViewModel styleViewModel = new StyleViewModel(style);
            return View(styleViewModel);
        }

        //[Authorize(Roles = "admin")]
        // GET: Actors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Style style = uow.StyleRepository.GetByID((int)id);
            if (style == null)
            {
                return HttpNotFound();
            }
            StyleViewModel styleViewModel = new StyleViewModel(style);
            return View(styleViewModel);
        }

        // POST: Genres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,CreatedAt,UpdatedAt")] StyleViewModel styleViewModel)
        {
            if (ModelState.IsValid)
            {
                Style style = Transform(styleViewModel);
                uow.StyleRepository.CreateOrUpdate(style);
                return RedirectToAction("Index");
            }

            return View(styleViewModel);
        }

        // POST: Genres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,CreatedAt,UpdatedAt")] StyleViewModel styleViewModel)
        {
            if (ModelState.IsValid)
            {
                Style style = Transform(styleViewModel);
                uow.StyleRepository.CreateOrUpdate(style);
                return RedirectToAction("Index");
            }
            return View(styleViewModel);
        }

        private Style Transform(StyleViewModel styleViewModel)
        {
            return new Style()
            {
                ID = styleViewModel.ID,
                Title = styleViewModel.Title,
                CreatedAt = styleViewModel.CreatedAt,
                UpdatedAt = styleViewModel.UpdatedAt
            };
        }

    }
}
