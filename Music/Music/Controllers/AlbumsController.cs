﻿namespace Music.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using DataAccess.Repositories;
    using DataStructure;
    using Music.Models.ViewModels;
    using MvcPaging;

    //[Authorize(Roles = "admin")]
    public class AlbumsController : BaseController<Album>
    {
        public AlbumsController() : base(UnitOfWork.Main.AlbumRepository) { }

        public ActionResult Index(int? page, string queryString)
        {
            List<Album> albums;

            if (queryString != null && queryString != "")
            {
                albums = uow.AlbumRepository.SearchBy(queryString);
            }
            else
            {
                albums = uow.AlbumRepository.GetAll();
            }

            List<AlbumViewModel> albumViewModels = AlbumViewModel.ToList(albums);

            int maxPages = this.CalculatePagesCount(albumViewModels);
            int currentPage = this.CalculateCurrentPageNumber(page, maxPages);

            ViewBag.Page = currentPage;
            ViewBag.Max = maxPages;

            return View(albumViewModels.ToPagedList(currentPage, pageSize));
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = uow.AlbumRepository.GetByID((int)id);
            if (album == null)
            {
                return HttpNotFound();
            }
            AlbumViewModel albumViewModel = new AlbumViewModel(album);
            return View(albumViewModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        //[Authorize(Roles = "admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = uow.AlbumRepository.GetByID((int)id);
            if (album == null)
            {
                return HttpNotFound();
            }
            AlbumViewModel albumViewModel = new AlbumViewModel(album);
            return View(albumViewModel);
        }

        //[Authorize(Roles = "admin")]
        // GET: Actors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = uow.AlbumRepository.GetByID((int)id);
            if (album == null)
            {
                return HttpNotFound();
            }
            AlbumViewModel albumViewModel = new AlbumViewModel(album);
            return View(albumViewModel);
        }

        // POST: Directors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,YearReleased,CreatedAt,UpdatedAt")] AlbumViewModel albumViewModel)
        {

            if (ModelState.IsValid)
            {
                Album album = Transform(albumViewModel);
                uow.AlbumRepository.CreateOrUpdate(album);
                return RedirectToAction("Index");
            }

            return View(albumViewModel);
        }

        // POST: Directors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,YearReleased,CreatedAt,UpdatedAt")] AlbumViewModel albumViewModel)
        {
            if (ModelState.IsValid)
            {
                Album album = Transform(albumViewModel);
                uow.AlbumRepository.CreateOrUpdate(album);
                return RedirectToAction("Index");
            }
            return View(albumViewModel);
        }

        private Album Transform(AlbumViewModel albumViewModel)
        {
            return new Album()
            {
                ID = albumViewModel.ID,
                Name = albumViewModel.Name,
                YearReleased = albumViewModel.YearReleased,
                CreatedAt = albumViewModel.CreatedAt,
                UpdatedAt = albumViewModel.UpdatedAt
            };
        }

    }
}
