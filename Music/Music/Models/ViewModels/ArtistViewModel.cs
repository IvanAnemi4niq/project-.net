﻿using DataStructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Music.Models.ViewModels
{
    public class ArtistViewModel : ViewModel
    {
        [Required]
        
        public string FirstName { get; set; }

        [Required]
      
        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        // MARK:- Many-to-Many Relationships

        public ArtistViewModel() : base() { }

        public ArtistViewModel(Artist artist) : base(artist)
        {
            FirstName = artist.FirstName;
            LastName = artist.LastName;
        }

        public static List<ArtistViewModel> ToList(List<Artist> artists)
        {
            List<ArtistViewModel> artistViewModels = new List<ArtistViewModel>();
            foreach (Artist artist in artists)
            {
                ArtistViewModel artistViewModel = new ArtistViewModel(artist);
                artistViewModels.Add(artistViewModel);
            }
            return artistViewModels;
        }
    }
}