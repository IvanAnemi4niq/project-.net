﻿using DataStructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Music.Models.ViewModels
{
    public class AlbumViewModel : ViewModel
    {
        [Required]
        
        public string Name { get; set; }

        [Required]

        public int YearReleased { get; set; }



        // MARK:- Many-to-Many Relationships

        public AlbumViewModel() : base() { }

        public AlbumViewModel(Album album) : base(album)
        {
            Name = album.Name;
            YearReleased = album.YearReleased;
        }

        public static List<AlbumViewModel> ToList(List<Album> albums)
        {
            List<AlbumViewModel> albumViewModels = new List<AlbumViewModel>();
            foreach (Album album in albums)
            {
                AlbumViewModel albumViewModel = new AlbumViewModel(album);
                albumViewModels.Add(albumViewModel);
            }
            return albumViewModels;
        }
    }
}