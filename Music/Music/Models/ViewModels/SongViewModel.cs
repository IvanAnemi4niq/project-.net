﻿namespace Music.Models.ViewModels
{
    using DataStructure;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    public class SongViewModel : ViewModel
    {
        [Required]
      
        public string Name { get; set; }

        [Required]
        public int Duration { get; set; }

        // MARK: - 1-to-1 Relationships

        public int AlbumID { get; set; }
        public AlbumViewModel Album { get; set; }

        public int StyleID { get; set; }
        public StyleViewModel Style { get; set; }

        // MARK: - Many-to-Many Relationships

        public List<int> ArtistIDs { get; set; }
        public ICollection<ArtistViewModel> Artists { get; set; }

        public SongViewModel() : base() { }

        public SongViewModel(Song song) : base(song)
        {
            Name = song.Name;
            Duration = song.Duration;

            if (song.Album != null)
            {
                AlbumID = song.Album.ID;
                Album = new AlbumViewModel(song.Album);
            }

            if (song.Style != null)
            {
                StyleID = song.Style.ID;
                Style = new StyleViewModel(song.Style);
            }

            if (song.Artists != null)
            {
                ArtistIDs = new List<int>();
                foreach (Artist artist in song.Artists)
                {
                    ArtistIDs.Add(artist.ID);
                }

                Artists = ArtistViewModel.ToList(song.Artists.ToList());
            }
        }

        public static List<SongViewModel> ToList(ICollection<Song> songs)
        {
            List<SongViewModel> songViewModels = new List<SongViewModel>();
            foreach (Song song in songs)
            {
                SongViewModel songViewModel = new SongViewModel(song);
                songViewModels.Add(songViewModel);
            }
            return songViewModels;
        }
    }
}