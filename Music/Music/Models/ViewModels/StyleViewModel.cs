﻿namespace Music.Models.ViewModels
{
    using DataStructure;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class StyleViewModel : ViewModel
    {
        [Required]
        public string Title { get; set; }

        public StyleViewModel() : base() { }

        public StyleViewModel(Style style) : base(style)
        {
            Title = style.Title;
        }

        public static List<StyleViewModel> ToList(List<Style> styles)
        {
            List<StyleViewModel> styleViewModels = new List<StyleViewModel>();
            foreach (Style style in styles)
            {
                StyleViewModel styleViewModel = new StyleViewModel(style);
                styleViewModels.Add(styleViewModel);
            }
            return styleViewModels;
        }
    }
}