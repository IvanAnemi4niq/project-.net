﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataStructure
{

    public class Album : Model
    {


        [Required]
        public string Name { get; set; }

        [Required]
        public int YearReleased { get; set; }

       
    }
}
