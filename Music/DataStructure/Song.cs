﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructure
{
    public class Song : Model
    {
        [Required]
        
        public string Name { get; set; }

        [Required]

        public int Duration { get; set; }
        public virtual Album Album { get; set; }
        public virtual ICollection<Artist> Artists { get; set; }
        public virtual Style Style { get; set; }
    }
}
