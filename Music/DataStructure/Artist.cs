﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructure
{
    public class Artist : Model
    {
        [Required]
       
        public string FirstName { get; set; }

        [Required]
        
        public string LastName { get; set; }
        public virtual ICollection<Song> Songs { get; set; }

    }
}
